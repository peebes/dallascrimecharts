# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20131001224739) do

  create_table "police_reports", :force => true do |t|
    t.string   "block"
    t.string   "street"
    t.string   "blockandstreet"
    t.string   "monthname"
    t.integer  "monthnum"
    t.integer  "daynum"
    t.integer  "yearnum"
    t.string   "dayofweek"
    t.text     "fulltext"
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "weekofyearstring"
    t.string   "typeofincident"
    t.date     "dateofincident"
    t.string   "checksumstring"
  end

  add_index "police_reports", ["checksumstring"], :name => "index_police_reports_on_checksumstring"
  add_index "police_reports", ["dateofincident"], :name => "index_police_reports_on_dateofincident"
  add_index "police_reports", ["typeofincident"], :name => "index_police_reports_on_typeofincident"

end
