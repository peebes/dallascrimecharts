class RenameDateOfCrimeToDateOfIncident < ActiveRecord::Migration
  def up
    remove_column :police_reports, :dateofcrime
    add_column :police_reports, :dateofincident, :date
  end

  def down
    remove_column :police_reports, :dateofincident
    add_column :police_reports, :dateofcrime, :date
  end
end
