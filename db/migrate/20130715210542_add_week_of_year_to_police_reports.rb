class AddWeekOfYearToPoliceReports < ActiveRecord::Migration
  def change
    add_column :police_reports, :weekofyearstring, :string
  end
end
