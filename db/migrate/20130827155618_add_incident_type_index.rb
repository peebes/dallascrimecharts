class AddIncidentTypeIndex < ActiveRecord::Migration
  def change
    add_index :police_reports, :typeofincident
  end
end
