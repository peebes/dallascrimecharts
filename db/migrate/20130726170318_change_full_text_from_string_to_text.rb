class ChangeFullTextFromStringToText < ActiveRecord::Migration
  def up
      change_column :police_reports, :fulltext, :text
  end
  def down
      # This might cause trouble if you have strings longer
      # than 255 characters.
      change_column :police_reports, :fulltext, :string
  end
end
