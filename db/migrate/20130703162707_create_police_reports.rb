class CreatePoliceReports < ActiveRecord::Migration
  def change
    create_table :police_reports do |t|
      t.string :typeofcrime
      t.string :block
      t.string :street
  		t.string :blockandstreet
      t.date   :dateofcrime
      t.string  :monthname
      t.integer :monthnum
      t.integer :daynum
      t.integer :yearnum
      t.string  :dayofweek
      t.string  :fulltext
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
  end
end
