class RenameTypeOfCrimeToTypeOfCall < ActiveRecord::Migration
  def up
    remove_column :police_reports, :typeofcrime
    add_column :police_reports, :typeofcall, :string
  end

  def down
    remove_column :police_reports, :typeofcall
    add_column :police_reports, :typeofcrime, :string
  end
end
