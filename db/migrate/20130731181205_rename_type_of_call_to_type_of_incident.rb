class RenameTypeOfCallToTypeOfIncident < ActiveRecord::Migration
  def up
    remove_column :police_reports, :typeofcall
    add_column :police_reports, :typeofincident, :string
  end

  def down
    remove_column :police_reports, :typeofincident
    add_column :police_reports, :typeofcall, :string
  end
end
