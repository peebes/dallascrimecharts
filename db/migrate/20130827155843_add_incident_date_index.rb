class AddIncidentDateIndex < ActiveRecord::Migration
  def change
    add_index :police_reports, :dateofincident
  end
end
