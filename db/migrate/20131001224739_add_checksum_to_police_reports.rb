class AddChecksumToPoliceReports < ActiveRecord::Migration
  def change
    add_column :police_reports, :checksumstring, :string
    add_index :police_reports, :checksumstring    
  end
end
