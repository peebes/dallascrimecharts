Dallascharts::Application.routes.draw do
#  get "buildchart/index"
#  get "buildheatmap/index"

  #match 'd3_charts/index' => 'd3_charts#chartform'
  #match 'geo_heat_maps/index' => 'd3_charts#chartform'
  #scope "/dallascharts" do
    # TODO: PDB: This is a pretty major hack
    match 'd3_charts/sunburst_help_1.png', :to => redirect("#{ActionController::Base.relative_url_root}/assets/sunburst_help_1.png")
    match 'd3_charts/sunburst_help_2.png', :to => redirect("#{ActionController::Base.relative_url_root}/assets/sunburst_help_2.png")
    match 'd3_charts/sunburst_help_3.png', :to => redirect("#{ActionController::Base.relative_url_root}/assets/sunburst_help_3.png")
    match 'd3_charts/sunburst_help_4.png', :to => redirect("#{ActionController::Base.relative_url_root}/assets/sunburst_help_4.png")
    match 'd3_charts/bar_help_1.png', :to => redirect("#{ActionController::Base.relative_url_root}/assets/bar_help_1.png")
    match 'd3_charts/bar_help_2.png', :to => redirect("#{ActionController::Base.relative_url_root}/assets/bar_help_2.png")
    match 'd3_charts/bar_help_3.png', :to => redirect("#{ActionController::Base.relative_url_root}/assets/bar_help_3.png")
    match '/heatmap_help_1.png', :to => redirect("#{ActionController::Base.relative_url_root}/assets/heatmap_help_1.png")
    #match '/assets', :to => redirect("#{ActionController::Base.relative_url_root}/assets")
    resources :d3_charts
    resources :geo_heat_maps

    match 'chartform' => 'd3_charts#chartform'
    match 'buildchart' => 'd3_charts#buildchart'
    match 'heatmapform' => 'geo_heat_maps#heatmapform'
    match 'buildheatmap' => 'geo_heat_maps#buildheatmap'

  #end
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
