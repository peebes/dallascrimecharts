class GeoHeatMapsController < ApplicationController
  # GET /geo_heat_maps
  # GET /geo_heat_maps.json
  def index
    @options_hash = fixup_options_hash params[:options]
    tempres = generate_heatmap_coordinate_string @options_hash
    @coord_string = tempres[0]
    @record_count = tempres[1]
    @max_occurence = tempres[2]

    # see if we have centerpoint and zoom information
    #PDB_TODO: This obviously should be tunable

    @center_lat = "32.7758"
    @center_lon = "-96.7967"
    @zoom_level = "10"
    @center_lat = params[:centerlat] if params.has_key? :centerlat
    @center_lon = params[:centerlon] if params.has_key? :centerlon
    @zoom_level = params[:zoomlevel] if params.has_key? :zoomlevel
    @title = params[:title]
    @notes = params[:notes]

    @map_width = 500
    if (@options_hash.has_key? :global) && (@options_hash[:global].has_key? :width)
      @map_width = @options_hash[:global][:width].to_i
    end

    @map_height = 400
    if (@options_hash.has_key? :global) && (@options_hash[:global].has_key? :height)
      @map_height = @options_hash[:global][:height].to_i
    end

    #setup maponly option
    @map_only = false
    if (@options_hash.has_key? :global) && (@options_hash[:global].has_key? :maponly)
      @map_only = true
    end

    #
    # Ok, let's build some string data about what generated here
    #
    #
    # I decided to narrow down to just the imporant information.
    #   Start/End dates, and fields that are used.
    #
    start_date = @options_hash[:global][:start_date]
    end_date = @options_hash[:global][:end_date]
    @query_parameter_array = []
    # Everything goes on in the form of Query Parm Name, Value
    @query_parameter_array << ["Start Date", "#{start_date}"]
    @query_parameter_array << ["End Date", "#{end_date}"]

    # PDBPDB TODO: Need to reduce duplication
    calltype_rlookup = {  
      "Dist/Miscf.".downcase => "Criminal Mischief/Valdalism",
      "AnmlBite".downcase => "Animal Bites",
      "Theft".downcase => "Theft",
      "AutoTheft".downcase => "Auto Theft-UUMV",
      "AcInj_PP".downcase => "Accidental Injury - Public Property",
      "Arson".downcase => "Arson AND Attempted Arson",
      "Burglary".downcase => "Burglary",
      "Disord".downcase => "Disorderly Conduct",
      "Embez".downcase => "Embezzlement",
      "FirearmAcc".downcase => "Firearms Accidents",
      "Forgery".downcase => "Forgery & Counterfeitig",
      "FoundProp".downcase => "Found Property",
      "Fraud".downcase => "Fraud",
      "HomeAcc".downcase => "Home Accidents",
      "LostProp".downcase => "Lost Property",
      "Robbery".downcase => "Robbery",
      "SudDeath/BodyFound".downcase => "Sudden Death-Bodies Found",
      "TrafMotVeh".downcase => "Traffic Motor Vehicle",
      "CwpChToWeap".downcase => "CWP CH To Weapon",
    }

    # see what included types we have
    included_type_count = 0
    if @options_hash[:typeofincident][:include]          
      temp_value_str = ""
      @options_hash[:typeofincident][:include].each { |term| 
        dcterm = term.downcase
        if calltype_rlookup.has_key? dcterm
          temp_value_str << ", " if included_type_count > 0 
          temp_value_str << calltype_rlookup[dcterm]
          included_type_count += 1
        else
          temp_value_str << ", " if included_type_count > 0 
          temp_value_str << dcterm
          included_type_count += 1          
        end
      }

      if temp_value_str.length > 0
        @query_parameter_array << ["Incident Types Mapped", temp_value_str]
      end
    end

    if @options_hash[:global][:text_search]
      @query_parameter_array << ["Text Search Terms", "#{@options_hash[:global][:text_search]}"]
    end

    @query_parameter_array << ["Max occurences at one location", "#{@max_occurence}"]
    @query_parameter_array << ["Record Count", "#{@record_count}"]

    render :layout => 'zerb_heatmapresults'   

  end

  def heatmapform
    @page_title = "Heatmap Builder"

    @avl_calltypes = [
                        ["Accidental Injury - Public Property", "AcInj_PP".capitalize],
                        ["Animal Bites", "AnmlBite".capitalize],
                        ["Arson AND Attempted Arson", "Arson".capitalize],
                        ["Auto Theft-UUMV", "AutoTheft".capitalize],
                        ["Burglary", "Burglary".capitalize],
                        ["Criminal Mischief/Valdalism", "Dist/Miscf.".capitalize],
                        ["CWP CH To Weapon", "CwpChToWeap".capitalize],
                        ["Disorderly Conduct", "Disord".capitalize],
                        ["Embezzlement", "Embez".capitalize],
                        ["Firearms Accidents", "FirearmAcc".capitalize],
                        ["Forgery & Counterfeiting", "Forgery".capitalize],
                        ["Found Property", "FoundProp".capitalize],
                        ["Fraud", "Fraud".capitalize],
                        ["Home Accidents", "HomeAcc".capitalize],
                        ["Lost Property", "LostProp".capitalize],
                        ["Robbery", "Robbery".capitalize],
                        ["Sudden Death-Bodies Found", "SudDeath/BodyFound".capitalize],
                        ["Theft", "Theft".capitalize],
                        ["Traffic Motor Vehicle", "TrafMotVeh".capitalize],
                     ]

    @map_sizes = [['500 Low Resolution (Tablet/Phone)', 500],
                    ['700 Medium Resolution (Tablet/Phone)', 700],
                    ['900 High Resolution (Desktop)', 900],
                    ['1100 - Very High Resolution (Desktop)', 1100]]



    # LEARNING: this needs to go last
    render :layout => 'zerb'
  end

  def buildheatmap
    # ok, we have been called by the chart builder, we need to take the inputs and convert it to
    # something that we can use
    @baseurl = geo_heat_maps_url
    chartparams = params[:buildheatmap] # because I'm lazy

    #
    # TODO: PDB - comon, you can get rid of this double iteration
    #
    incident_type_list = []
    @outgoing_url_params = []
    chartparams[:incident_types].each {|field|
      if field.size > 0
        incident_type_list << field
      end
    }

    if incident_type_list.count <= 0
      return redirect_to heatmapform_url , :flash => { :error => 'You Must Select at least one type of Incident' }
    end

    include_string = "options[typeofincident][include]="

    # ok, let's start picking this apart
    incident_type_list.each { |value|
      # if a field is being listed, we are going to assume the user doesn't want blank values
      include_string << "#{value},"
    }
    include_string.chop!    

    # see if we have a size parm
    if (chartparams.has_key? :chart_size) && (chartparams[:chart_size].to_i > 0)
      @outgoing_url_params << "options[global][width]=#{chartparams[:chart_size].to_i}"
      @outgoing_url_params << "options[global][height]=#{chartparams[:chart_size].to_i}"
    end

    # see if we have a title
    if chartparams.has_key? :chart_title
      @outgoing_url_params << "options[global][chart_title]=#{chartparams[:chart_title]}"      
    end

    if chartparams[:start_date]
      @outgoing_url_params << "options[global][start_date]=#{chartparams[:start_date]}"      
    else
      return redirect_to heatmapform_url, :flash => { :error => 'You Must Provide Start Date in Format mm-dd-yyyy' }
    end

    if chartparams[:end_date]
      @outgoing_url_params << "options[global][end_date]=#{chartparams[:end_date]}"
    else
      return redirect_to heatmapform_url, :flash => { :error => 'You Must Provide End Date in Format mm-dd-yyyy' }
    end

    # see if we are having to do a text search
    if chartparams[:text_search]&&chartparams[:text_search].strip.length > 0
      @outgoing_url_params << "options[global][text_search]=#{chartparams[:text_search]}"
    end

    @outgoing_url = @baseurl + "?"
    @outgoing_url << include_string
    @outgoing_url << "&"
    @outgoing_url << "options[typeofincident][exclude]=*&"

    title = ""
    if chartparams[:heatmap_title] && chartparams[:heatmap_title].length > 0
      title = chartparams[:heatmap_title]
      @outgoing_url_params << "title=#{title}"
    end

    notes = ""
    if chartparams[:heatmap_notes] && chartparams[:heatmap_notes].length > 0
      notes = chartparams[:heatmap_notes]
      @outgoing_url_params << "notes=#{notes}"
    end

    if notes.length == 0 && title.length == 0
      @outgoing_url_params << "options[global][maponly]"      
    end

    @outgoing_url_params.each { |parameter|
      @outgoing_url << parameter
      @outgoing_url << "&"
    }
    @outgoing_url.chop!

    redirect_to @outgoing_url
  end
  
  def generate_heatmap_coordinate_string options
    locationhash = {}

    locstring = ""

    maxval = 0

    # OK, let's build up a where portion of the query to make things go
    # a little faster.
    temprec = PoliceReport

    # see if we can build in date comparisons
    if options[:global][:start_date]
      temprec = temprec.where("dateofincident >= ?", options[:global][:start_date])
    end

    if options[:global][:end_date]
      temprec = temprec.where("dateofincident <= ?", options[:global][:end_date])
    end

    if options[:typeofincident][:include]          
      temprec = temprec.where(:typeofincident => options[:typeofincident][:include])
    end

    #PoliceReport.all.each { |x|    
    temprec.all.each { |x|

      # if this one got filtered, skip it
      next if is_filtered? x, options

      # weed out the "fallback" address when it cant look up the proper address
      if x.latitude && x.longitude && x.latitude != 32.781179 && x.latitude != 0
        hashkey = "#{x.latitude}#{x.longitude}"
        # see if we already have entry
        if !locationhash.has_key? hashkey
          # no entry for this location, add to hash
          locationhash[hashkey] = { :lat => x.latitude, :lon => x.longitude, :size => 0}    
        end

        # increment count for this location
        locationhash[hashkey][:size] += 1

        # the heatmap cares about the maximum value
        maxval = [maxval, locationhash[hashkey][:size]].max
      end
    }
    # ok, we have compiled all the information for the reports, build up the string
    locstring = "max: #{maxval}, data: ["
    locationhash.each { |key, data_hash|
        if data_hash[:size] >= 1
          locstring += "," if locstring.size > 20
          # locstring << "{ lat:" << data_hash[:lat].to_s << ", lon:" << data_hash[:lon].to_s << ", count:" << data_hash[:size].to_s << "}"
          locstring << "{lat:%0.6f,lon:%0.6f,count:%i}" % [data_hash[:lat], data_hash[:lon], data_hash[:size].to_s]
        end 
    }
    locstring << "]"

    [locstring, temprec.size, maxval]
  end  
end
