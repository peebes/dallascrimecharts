class ApplicationController < ActionController::Base
  #protect_from_forgery

	# == Method to take a database record and check against filter criteria
	#
	# supported operations
	# [:include] - specifically include a report that matches the filter spec
	# [:exclude] - specifically exclude a report that matches the filter spec
	#
	# if the filter spec is a string, it will test inclusion of the filter spec
	# if the filter spec is a numeric, it will test for equality
	#
	# returns true if filtered out, false if otherwise
	#
	def is_filtered? data_record, options
		## CHECK DATE FILTERS FIRST
	  ## THEN INCLUSION FILTERS.
	  ##   INCLUSION ALWAYS GETS PRIORITY OVER EXCLUSION
	  if options[:global]
		  if options[:global].has_key? :start_date
		  	# ok, we have a start date, now we have to parse it
		    start_date = DateTime.strptime(options[:global][:start_date], "%m/%d/%Y").to_date
		    if start_date && data_record.dateofincident
		    	# PDB - NOTE:!! TODO: I am making this data specific :( fix
		    	if data_record.dateofincident < start_date
		    		return true # you just been filtere!!!
		    	end
		    end
		  end

		  if options[:global].has_key? :end_date
		  	# ok, we have a start date, now we have to parse it
		    end_date = DateTime.strptime(options[:global][:end_date], "%m/%d/%Y").to_date
		    if end_date && data_record.dateofincident
		    	# PDB - NOTE:!! TODO: I am making this data specific :( fix
		    	if data_record.dateofincident > end_date
		    		return true # you just been filtere!!!
		    	end
		    end
		  end
		end

		## see if we need to do a text search
	  if options[:global] && options[:global][:text_search] && data_record.fulltext
	  	text_searches = options[:global][:text_search].downcase.split",".strip
	  	matched = false
	  	text_searches.each{ |text|
		  	matched = true if text.length > 0 && data_record.fulltext.downcase.include?(text)
		  	# NOTE: other criteria can still fail you even if text search matches
		  }
		  return true if !matched
	  end

	  options.each {|field, option_hash|
	    if option_hash.has_key? :include
	      if match_filter_spec? data_record, field, option_hash[:include]
	        return false
	      end
	    end
	  }
	  options.each {|field, option_hash|
	    if option_hash.has_key? :exclude
	      if match_filter_spec? data_record, field, option_hash[:exclude]
	        return true
	      end
	    end
	  }

	  false # default to not filtered
	end

	#
	# == Test a specific set of filter specs against data record to see if content matches specs
	#
	# PDB TODO: Come back and setup for wildcarding or even better regex matching
	def match_filter_spec?(data_record, field, filter_spec_list)
	  # for each filter specification (you can have multiple)
	  filter_spec_list.each do |filter|
	  	# if you specify wildcard, instant match
	  	return true if filter == "*"

	  	# level the playing field
	  	filter.downcase!

	    if !data_record[field].nil?
	      if data_record[field].class.to_s == "String"
	        # if the target data is string, do a include test on to_s of filter spec
	        if data_record[field].downcase.include? filter.to_s.downcase
	          return true # matched filter
	        end
	      else
	        # if the target data is not string (numeric, otherwise), do a simple equality check
	        if data_record[field] == filter
	          return true# matched filter
	        end
	      end
	    end
	  end

	  false # get here, you didn't match
	end

	#PDB TODO: factor out duplication
	def fixup_options_hash options_hash
    options_hash = {} if options_hash.nil?

    # convert string keys and nested string keys into symbols
    options_hash = options_hash.inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}
    new_options_hash = {}
    # make sure mincount is integer
    options_hash.each {|key, value|
      new_options_hash[key] = value.inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}
      new_options_hash[key][:mincount] = new_options_hash[key][:mincount].to_i if new_options_hash[key].has_key? :mincount
      new_options_hash[key][:allow_blank] = new_options_hash[key][:allow_blank].downcase != "false" if new_options_hash[key].has_key? :allow_blank
      if new_options_hash[key].has_key? :include
      	new_options_hash[key][:include] = new_options_hash[key][:include].split(",")
      end

      if new_options_hash[key].has_key? :exclude
      	new_options_hash[key][:exclude] = new_options_hash[key][:exclude].split(",")
      end

    }

    new_options_hash
  end
end
