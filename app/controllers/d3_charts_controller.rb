class D3ChartsController < ApplicationController

  include D3ChartsHelper
  # GET /d3_charts
  # GET /d3_charts.json
  def index
    #render :layout => 'zerb'    
    #
    # Probably not doing this the "Rails Way"
    #
    redirect_to chartform_url

    #build_chart_variables

    #@json_data = "some data"
    #finalhash = build_data_hierarch_hash_for_d3 PoliceReport, field_array, options_hash
  end

  # GET /d3_charts/1
  # GET /d3_charts/1.json
  def show
    build_chart_variables
    @chart_type = params[:id]
    render :layout => 'zerb_chartresults'    
  end

  def chartform
    @page_title = "Chart Builder"
    @avl_fields = [["Type Of Incident", :typeofincident],
                    ["Block", :block],
                    ["Street", :street],
                    ["Street And Block", :blockandstreet],
                    ["Date Of Call", :dateofincident],                    
                    ["Month", :monthname],
                    ["Week Of Year", :weekofyearstring],
                    ["Day of Week", :dayofweek],
                    ["Year", :yearnum]]    

    @avl_calltypes = [
                        ["Accidental Injury - Public Property", "AcInj_PP".capitalize],
                        ["Animal Bites", "AnmlBite".capitalize],
                        ["Arson AND Attempted Arson", "Arson".capitalize],
                        ["Auto Theft-UUMV", "AutoTheft".capitalize],
                        ["Burglary", "Burglary".capitalize],
                        ["Criminal Mischief/Valdalism", "Dist/Miscf.".capitalize],
                        ["CWP CH To Weapon", "CwpChToWeap".capitalize],
                        ["Disorderly Conduct", "Disord".capitalize],
                        ["Embezzlement", "Embez".capitalize],
                        ["Firearms Accidents", "FirearmAcc".capitalize],
                        ["Forgery & Counterfeiting", "Forgery".capitalize],
                        ["Found Property", "FoundProp".capitalize],
                        ["Fraud", "Fraud".capitalize],
                        ["Home Accidents", "HomeAcc".capitalize],
                        ["Lost Property", "LostProp".capitalize],
                        ["Robbery", "Robbery".capitalize],
                        ["Sudden Death-Bodies Found", "SudDeath/BodyFound".capitalize],
                        ["Theft", "Theft".capitalize],
                        ["Traffic Motor Vehicle", "TrafMotVeh".capitalize],
                     ]

    @avl_charts = [["Sunburst", :sunburst],
                   ["Bar", :hierchbar]] #,
                   #["Tree", :iztreegraph]]

    @chart_sizes = [['500 Low Resolution (Tablet/Phone)', 500],
                    ['700 Medium Resolution (Tablet/Phone)', 700],
                    ['900 High Resolution (Desktop)', 900],
                    ['1100 - Very High Resolution (Desktop)', 1100]]

    # this needs to go last
    #render :layout => 'multiselect'
    #render :layout => 'multiselect6One3'    
    render :layout => 'zerb'    

  end

  def buildchart
    # ok, we have been called by the chart builder, we need to take the inputs and convert it to
    # something that we can use
    chartparams = params[:buildchart] # because I'm lazy
    @baseurl = d3_charts_url

    @charttype = chartparams[:chart_type]    

    if @charttype.nil? || @charttype.size <= 0
      return redirect_to chartform_url, :flash => { :error => 'You Must Supply a Chart Type' }
    end

    #
    # TODO: PDB - comon, you can get rid of this double iteration
    #
    field_list = []
    @outgoing_url_params = []
    chartparams[:db_fields].each {|field|
      if field.size > 0
        field_list << field
      end
    }

    incident_type_list = []
    chartparams[:incident_types].each {|field|
      if field.size > 0
        incident_type_list << field
      end
    }


    if field_list.count <= 0
      return redirect_to chartform_url, :flash => { :error => 'You Must Select Fields for the report' }
    end

    # we may or may not have anything for the minimum count
    min_count = chartparams[:min_count]
    if !min_count.nil? && min_count.size > 0
      if min_count.to_i <= 0
        return redirect_to chartform_url, :flash => { :error => 'Minimum Count must be blank or integer > 0'}
      else
        @outgoing_url_params << "options[#{field_list[0]}][mincount]=#{min_count.to_i}"
      end
    end

    # see if we have a size parm
    if (chartparams.has_key? :chart_size) && (chartparams[:chart_size].to_i > 0)
      @outgoing_url_params << "options[global][width]=#{chartparams[:chart_size].to_i}"
      @outgoing_url_params << "options[global][height]=#{chartparams[:chart_size].to_i}"
    end

    # see if we have a title
    if chartparams.has_key? :chart_title
      @outgoing_url_params << "options[global][chart_title]=#{chartparams[:chart_title]}"      
    end

    if chartparams[:start_date]
      @outgoing_url_params << "options[global][start_date]=#{chartparams[:start_date]}"      
    else
      return redirect_to chartform_url, :flash => { :error => 'You Must Provide Start Date in Format mm-dd-yyyy' }
    end

    if chartparams[:end_date]
      @outgoing_url_params << "options[global][end_date]=#{chartparams[:end_date]}"
    else
      return redirect_to chartform_url, :flash => { :error => 'You Must Provide End Date in Format mm-dd-yyyy' }
    end

    # now that we have valid fields
    #   build up the field part of the url

    # ok, let's start picking this apart
    field_list.each_with_index { |value, index|
      # add the list to the fields to use, in the order the user had
      @outgoing_url_params << "fields[#{index}]=#{value}"

      # if a field is being listed, we are going to assume the user doesn't want blank values
      @outgoing_url_params << "options[#{value}][allow_blank]=false"
    }

    # if hierarch bar, no prepended counts, its the point of a bar graph
    if @charttype.to_s == "hierchbar"
      @outgoing_url_params << "options[global][noprependcount]"
    end

    # see if we are having to do a text search
    if chartparams[:text_search]&&chartparams[:text_search].strip.length > 0
      @outgoing_url_params << "options[global][text_search]=#{chartparams[:text_search]}"
    end

    include_string = "options[typeofincident][include]="
    incident_type_list.each { |value|
      # if a field is being listed, we are going to assume the user doesn't want blank values
      include_string << "#{value},"
    }
    include_string.chop!    
    @outgoing_url_params.unshift(include_string)

    # ok, build up our string
    @outgoing_url = @baseurl + "/" + @charttype + "?"
    @outgoing_url_params.each { |parameter|
      @outgoing_url << parameter
      @outgoing_url << "&"
    }
    @outgoing_url.chop!

    redirect_to @outgoing_url
  end

  # PDB TODO: This is really doing more than building_varibles. Rename
  def build_chart_variables
      # PDBPDB TODO: Need to reduce duplication
    calltype_rlookup = {  
      "Dist/Miscf.".downcase => "Criminal Mischief/Valdalism",
      "AnmlBite".downcase => "Animal Bites",
      "Theft".downcase => "Theft",
      "AutoTheft".downcase => "Auto Theft-UUMV",
      "AcInj_PP".downcase => "Accidental Injury - Public Property",
      "Arson".downcase => "Arson AND Attempted Arson",
      "Burglary".downcase => "Burglary",
      "Disord".downcase => "Disorderly Conduct",
      "Embez".downcase => "Embezzlement",
      "FirearmAcc".downcase => "Firearms Accidents",
      "Forgery".downcase => "Forgery & Counterfeitig",
      "FoundProp".downcase => "Found Property",
      "Fraud".downcase => "Fraud",
      "HomeAcc".downcase => "Home Accidents",
      "LostProp".downcase => "Lost Property",
      "Robbery".downcase => "Robbery",
      "SudDeath/BodyFound".downcase => "Sudden Death-Bodies Found",
      "TrafMotVeh".downcase => "Traffic Motor Vehicle",
      "CwpChToWeap".downcase => "CWP CH To Weapon",
    }
  
    # first, get the fields for the display
    @field_array = []
    params[:fields].keys.map {|x| x.to_i}.sort.each { |sorted_index|
      @field_array << params[:fields][sorted_index.to_s].to_sym 
    }

    @options_hash = params[:options]
    @options_hash = {} if @options_hash.nil?

    # convert string keys and nested string keys into symbols
    #PDB TODO: Comon pressly, move to the common call!!!!
    @options_hash = @options_hash.inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}
    new_options_hash = {}
    # make sure mincount is integer
    @options_hash.each {|key, value|
      new_options_hash[key] = value.inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}
      new_options_hash[key][:mincount] = new_options_hash[key][:mincount].to_i if new_options_hash[key].has_key? :mincount
      new_options_hash[key][:allow_blank] = new_options_hash[key][:allow_blank].downcase != "false" if new_options_hash[key].has_key? :allow_blank

      if new_options_hash[key].has_key? :include
        new_options_hash[key][:include] = new_options_hash[key][:include].split(",")
      end      

      if new_options_hash[key].has_key? :exclude
        new_options_hash[key][:exclude] = new_options_hash[key][:exclude].split(",")
      end      
    }
    
    @options_hash = new_options_hash

    # PDB TODO: Make customizable chart size defaults for each chart type
    @chart_height = 800
    if (@options_hash.has_key? :global) && (@options_hash[:global].has_key? :height)
      @chart_height = @options_hash[:global][:height].to_i
    end

    @chart_width = 800
    if (@options_hash.has_key? :global) && (@options_hash[:global].has_key? :width)
      @chart_width = @options_hash[:global][:width].to_i
    end

    # chart title
    @chart_title = ""
    if (@options_hash.has_key? :global) && (@options_hash[:global].has_key? :chart_title)
      @chart_title = @options_hash[:global][:chart_title]
    end

    @json_data = generate_d3_json PoliceReport, @field_array, @options_hash

    #
    # Ok, let's build some string data about what generated here
    #
    #
    # I decided to narrow down to just the imporant information.
    #   Start/End dates, and fields that are used.
    #
    start_date = @options_hash[:global][:start_date]
    end_date = @options_hash[:global][:end_date]

    @query_parameter_array = []
    # Everything goes on in the form of Query Parm Name, Value
    @query_parameter_array << ["Start Date", "#{start_date}"]
    @query_parameter_array << ["End Date", "#{end_date}"]

    # ok, now get fields
    #
    # PDB: TODO - Put this in a more centrailized location 
    @field_lookup = {:typeofincident => "Type Of Incident",
                     :block => "Block",
                     :street => "Street",
                     :blockandstreet => "Street And Block",
                     :dateofincident => "Date Of Call",                    
                     :monthname => "Month",
                     :weekofyearstring => "Week Of Year",
                     :dayofweek => "Day of Week",
                     :yearnum => "Year"}    

    # build up the part of the string that takes 
    field_index = 0
    temp_value_str = ""
    curr_field = params[:fields][field_index.to_s]
    first_field_min_count = @options_hash[curr_field.to_sym][:mincount] if curr_field
    while curr_field
        temp_value_str << ", " if field_index > 0 
        temp_value_str << @field_lookup[curr_field.to_sym]
        field_index += 1
        curr_field = params[:fields][field_index.to_s]
    end

    if temp_value_str.length > 0
      @query_parameter_array << ["Fields in order of use", temp_value_str]
    end    

    # see what included types we have
    included_type_count = 0
    if @options_hash[:typeofincident][:include]          
      temp_value_str = ""
      @options_hash[:typeofincident][:include].each { |term| 
        dcterm = term.downcase
        if calltype_rlookup.has_key? dcterm
          temp_value_str << ", " if included_type_count > 0 
          temp_value_str << calltype_rlookup[dcterm]
          included_type_count += 1
        else
          temp_value_str << ", " if included_type_count > 0 
          temp_value_str << dcterm
          included_type_count += 1          
        end
      }

      if temp_value_str.length > 0
        @query_parameter_array << ["Incident Types Mapped", temp_value_str]
      end
    end

    if first_field_min_count
      @query_parameter_array << ["Minimum Number of Incidents", "#{first_field_min_count}"]
    end

    if @options_hash[:global][:text_search]
      @query_parameter_array << ["Text Search Terms", "#{@options_hash[:global][:text_search]}"]
    end
  end
end
