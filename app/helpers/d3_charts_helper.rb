module D3ChartsHelper
	#
	# This method does all the big work
	#  it takes the lists of fields to includes and options
	#
	def build_data_hierarch_hash_for_d3 active_record_class, field_list, options
	  myhash = {:name => "", :children => {}}

	  temprec = active_record_class
	  # for each database object
    # see if we can build in date comparisons
    if options[:global][:start_date]
      temprec = temprec.where("dateofincident >= ?", options[:global][:start_date])
    end

    if options[:global][:end_date]
      temprec = temprec.where("dateofincident <= ?", options[:global][:end_date])
    end

    if options[:typeofincident][:include]          
      temprec = temprec.where(:typeofincident => options[:typeofincident][:include])
    end

	  # active_record_class.all.each { |x|
    temprec.all.each { |x|

	    # if this one got filtered, skip it
	    next if is_filtered? x, options

	    temphash = myhash
	    toplevelhash = myhash
	    index = 0
	    # Run through all the specified fields for each database record
	    while index < field_list.count
	      field = field_list[index] # get the field name
	      field_value = x[field] # get the field value

	      # see if field is blank and if we need to block it (because allow_block turned off in options)
	      if field_value.nil? || ((field_value.respond_to? :empty) && (field_value.empty?))
	        if (options.has_key? field) && (options[field].has_key? :allow_blank) && (!options[field][:allow_blank])
	          break # break out of while, we are done
	        end
	      end

	      # we have to treat the last field data special (the bottom of hierarcy is different)
	        bottom_level = index == field_list.count - 1

	        # if you don't already have a child element for this field, add it
	        if !temphash[:children].has_key? field_value
	          temphash[:children][field_value] = {:name => field_value.to_s, :field => field, :size => 0}
	          # need to add child hash if not bottom levl
	          temphash[:children][field_value][:children] = {} if !bottom_level
	        end

	        # always bump the count for this child level
	        temphash[:children][field_value][:size] += 1
	        if index != field_list.count - 1
	          # we are not at bottom of hierarchy, add children hash
	          #temphash[:children][field_value][:children] = {}
	          temphash = temphash[:children][field_value] if !bottom_level # point to next level of hierarch for next iter
	        end

	      index += 1
	    end
	  }

	  # now we have to convert the "children hashes to arrays, yuck"
	  # needed the children stored in a hash for efficient access. However,
	  #   the D3 library wants a child collection (array), so I have to convert
	  children_hash_to_array_with_min_filtering myhash, options
	end

	#
	# == this method convers the children hashes we like to the children
	#   array that D3 expects.
	#
	#  Note that I muddled the water a bit by doing some size filtering
	#    It is more effiecient this way because we are already iterating
	#    through the structure
	#
	def children_hash_to_array_with_min_filtering(childrenhash, option_hash)
	  if childrenhash && childrenhash.class.to_s == "Hash"
	    if childrenhash.has_key? :children
	      # ok, if we get here, we acutally have a children hash to deal with
	      childrenhash[:children] = childrenhash[:children].values # get children hashes
	      goodchildren = [] # goodchildren have passed the size checks
	      childrenhash[:children].each {|x|
	        if x.has_key?(:size) && (!x[:name].nil?)
	          field_options = option_hash[x[:field]]
	          if !field_options.nil? && (field_options.has_key? :mincount)
	            if x[:size] >= field_options[:mincount]
	              goodchildren << x
	            end
	          else
	            goodchildren << x
	          end
	        
	        	unless (option_hash.has_key? :global) && (option_hash[:global].has_key? :noprependcount)
	          	x[:name] =  "(#{x[:size]}) " + x[:name]
	          end

	        end
	        # since each children hash can have children of its own, we 
	        #   have to recursively call down to bottom of hierarchy
	        children_hash_to_array_with_min_filtering x, option_hash 
	      }
	      childrenhash[:children] = goodchildren
	    else
	      childrenhash = childrenhash.values
	    end
	  end

	  childrenhash
	end

	def generate_d3_json active_record_class, field_array, options_hash
	  finalhash = build_data_hierarch_hash_for_d3 PoliceReport, field_array, options_hash
	  auto_trim_hash finalhash, options_hash
		finalhash.to_json.html_safe
		#JSON.pretty_generate(finalhash).html_safe
	end

	def auto_trim_hash results_hash, options_hash
		# PDB: TODO - You need to make this more refined before actually using
		return

		size_count_hash = {}
		results_hash[:children].each { |elem|
			if !size_count_hash.has_key? elem[:size]
					size_count_hash[elem[:size]] = 0
			end

			size_count_hash[elem[:size]] += 1			
		}

		new_arr = size_count_hash.sort_by {|value, count| value * -1}

		low = 20
		low_limit = 0
		high_limit = 0
		high = 30
		sum = 0
		new_arr.each { |subarr|
			if sum < 15 and (sum + subarr[1]) >= 15
				low_limit = subarr[0]
			end

			if sum < 25 and (sum + subarr[1]) >= 25
				high_limit = subarr[0]
			end

			sum += subarr[1]
		}

#		puts "%%%%%%% #{high_limit} -- #{low_limit} %%%%%%%"

		# PDB Another spin through! you got to be kidding me
		final_arr = []
		results_hash[:children].each { |elem|
			if elem[:size] >= low_limit
				final_arr << elem
			end
		}
		results_hash[:children] = final_arr

	end
end
