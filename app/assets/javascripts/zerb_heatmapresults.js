
//= require jquery
//= require ../../../vendor/assets/javascripts/OpenLayers.min.js
//= require ../../../vendor/assets/javascripts/heatmap.min.js
//= require ../../../vendor/assets/javascripts/heatmap-openlayers.min.js
//= require ../../../vendor/assets/javascripts/heatmap-openlayers-renderer.min.js
//= require ../../../vendor/assets/javascripts/foundation.min.js
//= require ../../../vendor/assets/javascripts/custom.modernizr.js
//= require cloudmade_layer.min.js
