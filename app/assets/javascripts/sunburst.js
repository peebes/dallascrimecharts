
var x = d3.scale.linear()
    .range([0, 2.0 * Math.PI]);

var y = d3.scale.linear()
    .range([0, radius]);


var color = d3.scale.category20c();

var svg = d3.select("div#chartcontent").append("svg")
    .attr("width", width + 20)  // add some fudge factor (getting flat edge)
    .attr("height", height + 20) // add some fudge factor (getting flat edge)
    .attr("style", "display: block; margin: auto;")
  .append("g")
    .attr("transform", "translate(" + width / 2 + "," + (height / 2 + 10) + ")");

var partition = d3.layout.partition()
    .value(function(d) { return d.size; });

var arc = d3.svg.arc()
    .startAngle(function(d) { return Math.max(0, Math.min(2 * Math.PI, x(d.x))); })
    .endAngle(function(d) { return Math.max(0, Math.min(2 * Math.PI, x(d.x + d.dx))); })
    .innerRadius(function(d) { return Math.max(0, d.y ? y(d.y) : d.y); })
    .outerRadius(function(d) { return Math.max(0, y(d.y + d.dy)); });

var g = svg.selectAll("g")
    .data(partition.nodes(jsonBlob))
  .enter().append("g");

var path = g.append("path")
  .attr("d", arc)
  .style("fill", function(d) { return color((d.children ? d : d.parent).name); })
  .on("click", click);

//var textarr =strSplitOnLength(d.name, 13);

var text = g.append("text")
  .attr("transform", function(d) { return "rotate(" + computeTextRotation(d) + ")"; })
  .attr("x", function(d) { return y(d.y); })
  .attr("dx", "2") // margin
  .attr("dy", ".05em") // vertical-align
  .text(function(d) { return splitTextLine1(d.name); });

var textline2 = g.append("text")
  .attr("transform", function(d) { return "rotate(" + computeTextRotation(d) + ")"; })
  .attr("x", function(d) { return y(d.y); })
  .attr("dx", "6") // margin
  .attr("dy", "1.1em") // vertical-align
  .text(function(d) { return splitTextLine2(d.name); });
  //.text(function(d) { return "hello"; });

function click(d) {
  // fade out all text elements
  //text.transition().attr("opacity", 0);
  text.transition().attr("visibility", "hidden");
  textline2.transition().attr("visibility", "hidden");
  // PDB - opacity is a BAD idea, because the hidden text still intercepts click events
  //textline2.transition().attr("opacity", 0);

  // PDB - This is an extreme hack, but the x was creeping over into another data section,
  //   causing all kinds of crazyness
  d.x = d.x - .0000001

  path.transition()
    .duration(750)
    .attrTween("d", arcTween(d))
    .each("end", function(e, i) {
        // check if the animated element's data e lies within the visible angle span given in d
        // PDB: another stupid edge case fix. When zooming back out, first wedge usually
        // did not get text
        if (e.x + 0.0001 >= d.x && e.x < (d.x + d.dx)) {
          // get a selection of the associated text element
          var arcText = d3.select(this.parentNode).selectAll("text");
          // fade in the text element and recalculate positions
          arcText.transition().duration(750)
//            .attr("opacity", 1) // PDB - opacity is a BAD idea, because the
// hidden text still intercepts click events
            .attr("visibility", "visible")
            .attr("transform", function() { return "rotate(" + computeTextRotation(e) + ")" })
            .attr("x", function(d) { return y(d.y); });
        }
    });
}

d3.select(self.frameElement).style("height", height + "px");

// Interpolate the scales!
function arcTween(d) {
  var xd = d3.interpolate(x.domain(), [d.x, d.x + d.dx]),
      yd = d3.interpolate(y.domain(), [d.y, 1]),
      yr = d3.interpolate(y.range(), [d.y ? 20 : 0, radius]);
  return function(d, i) {
    return i
        ? function(t) { return arc(d); }
        : function(t) { x.domain(xd(t)); y.domain(yd(t)).range(yr(t)); return arc(d); };
  };
}

function computeTextRotation(d) {
  return (x(d.x + d.dx / 2) - Math.PI / 2) / Math.PI * 180;
}


function strSplitOnLength(data, your_desired_width) {
    if(data.length <= 0)
        return [];  // return an empty array

    var splitData = data.split(/([\s\n\r]+)/);
    var arr = [];
    var cnt = 0;
    for (var i = 0; i < splitData.length; ++i) {
        if (!arr[cnt]) arr[cnt] = '';  //Instantiate array entry to empty string if null or undefined

        if (your_desired_width < (splitData[i].length + arr[cnt].length))
        {
            cnt++;
            arr[cnt] = '';
        }

        arr[cnt] += splitData[i];
    }

    return arr;
}

function splitTextLine1(text){
  var text_array = strSplitOnLength(text, 16);
  if (text_array.length > 0)
  {
    return $.trim(text_array[0]);
  }

  return "";
}

function splitTextLine2(text){
  var text_array = strSplitOnLength(text, 16);
  if (text_array.length > 1)
  {
    return $.trim(text_array.splice(1,1).join(" "));
  }  

  return "";
}
