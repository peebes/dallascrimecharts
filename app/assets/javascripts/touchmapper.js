  var TouchHandler = function(){
    var touchHandled = false;
    var _touchMoved = false;

    var refiredTouchStart = false;
    var lastTouchStart = null;

    var originator;

    var mouseUpBlock = 0;
    var mouseDownBlock = 0;
    var clickBlock = 0;

    simulateMouseEvent = function(event, simulatedType) {

      // Ignore multi-touch events
      if (event.touches.length > 1) {
        return;
      }
      //event.preventDefault();

      var touch = event.changedTouches[0],
          simulatedEvent = document.createEvent('MouseEvents');

      var SX = touch.screenX;
      var SY = touch.screenY;
      var CX = touch.clientX;
      var CY = touch.clientY;
      //log.info (SX, "|",SY, "|",CX, "|",CY, "|")

      
      // Initialize the simulated mouse event using the touch event's coordinates
      simulatedEvent.initMouseEvent(
        simulatedType,    // type
        true,             // bubbles                    
        true,             // cancelable                 
        window,           // view                       
        1,                // detail                     
        SX,    // screenX                    
        SY,    // screenY                    
        CX,    // clientX                    
        CY,    // clientY                    
        false,            // ctrlKey                    
        false,            // altKey                     
        false,            // shiftKey                   
        false,            // metaKey                    
        0,                // button                     
        null              // relatedTarget              
      );

      // Dispatch the simulated event to the target element
      event.target.dispatchEvent(simulatedEvent);      
      //log.info ("simulatedMouseEvent: ", simulatedType);

    };

    this.touchStart = function(event) {
      // bail on multitouch events
      //log.info(event.targetTouches.length);
      if(event.targetTouches.length > 1)
      {
        return;
      }

      // notice this is hard bound to multiselect
      if (!findParentNode("uix-multiselect ui-widget", event.target)){
        return;
      }
/*
      // this was the old way, but ping zoom, and drag move of the web page did not
      //   work

      // If it is an input element, bail and pass up the chain
      if (/HTMLInputElement/.test(event.target.constructor.toString()))
      {
        return;
      }
*/

      var self = this;
      // Ignore the event if another widget is already being handled
      //if (touchHandled || !self._mouseCapture(event.changedTouches[0])) {
      if (touchHandled) {
        event.returnValue = true;
        return;
      }

      event.preventDefault();

      // Set the flag to prevent other widgets from inheriting the touch event
      touchHandled = true;

      // Track movement to determine if interaction was a click
      _touchMoved = false;

      // Simulate the mouseover event
      simulateMouseEvent(event, 'mouseover');

      // Simulate the mousemove event
      simulateMouseEvent(event, 'mousemove');

      // Simulate the mousedown event
      simulateMouseEvent(event, 'mousedown');      

      lastTouchStart = event;

      // log.info ("touchStart event.returnValue:  ", event.returnValue);
    };

    this.touchMove = function(event) {
      if(event.targetTouches.length > 1)
      {
        return;
      }

      // Ignore event if not handled
      if (!touchHandled){
        return;
      }

      event.preventDefault();

      // Interaction was not a click
      _touchMoved = true;

      // Simulate the mousemove event
      simulateMouseEvent(event, 'mousemove');
      //log.info ("touchMove event.returnValue:  ", event.returnValue);
    };

    this.touchEnd = function(event) {
      if(event.targetTouches.length > 1)
      {
        return;
      }

      // Ignore event if not handled
      if (!touchHandled) {
        return;
      }

        // Simulate the mouseup event
        simulateMouseEvent(event, 'mouseup');

        // Simulate the mouseout event
        simulateMouseEvent(event, 'mouseout');

      // If the touch interaction did not move, it should trigger a click
      if (!_touchMoved) {
        // Simulate the click event
        //log.info("simulating click");        
        simulateMouseEvent(event, 'click');
      }

      // Unset the flag to allow other widgets to inherit the touch event
      touchHandled = false;
      //log.info ("touchEnd event.returnValue:  ", event.returnValue);
    };

    // PDB: This is probably not the most efficient thing in the world, but hey
    function findParentNode(targetClassName, currentObj) {
        var testObj = currentObj.parentNode;
        var found = false;

        while(testObj.className != targetClassName && testObj != null) {
            testObj = testObj.parentNode;
        }

        if (testObj == null){
          return false;
        }

        return true;
    };    

  }

  touchHandler = new TouchHandler();
  document.addEventListener("touchstart", touchHandler.touchStart, true);
  document.addEventListener("touchmove", touchHandler.touchMove, true);
  document.addEventListener("touchend", touchHandler.touchEnd, true);
  // log.info ("Setup Handlers");
