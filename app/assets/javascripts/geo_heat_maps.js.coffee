# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

//= require ../../../vendor/assets/javascripts/OpenLayers.min.js
//= require ../../../vendor/assets/javascripts/heatmap.min.js
//= require ../../../vendor/assets/javascripts/heatmap-openlayers.min.js
//= require ../../../vendor/assets/javascripts/heatmap-openlayers-renderer.min.js
