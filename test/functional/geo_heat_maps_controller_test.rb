require 'test_helper'

class GeoHeatMapsControllerTest < ActionController::TestCase
  setup do
    @geo_heat_map = geo_heat_maps(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:geo_heat_maps)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create geo_heat_map" do
    assert_difference('GeoHeatMap.count') do
      post :create, geo_heat_map: {  }
    end

    assert_redirected_to geo_heat_map_path(assigns(:geo_heat_map))
  end

  test "should show geo_heat_map" do
    get :show, id: @geo_heat_map
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @geo_heat_map
    assert_response :success
  end

  test "should update geo_heat_map" do
    put :update, id: @geo_heat_map, geo_heat_map: {  }
    assert_redirected_to geo_heat_map_path(assigns(:geo_heat_map))
  end

  test "should destroy geo_heat_map" do
    assert_difference('GeoHeatMap.count', -1) do
      delete :destroy, id: @geo_heat_map
    end

    assert_redirected_to geo_heat_maps_path
  end
end
