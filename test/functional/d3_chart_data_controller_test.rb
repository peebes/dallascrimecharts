require 'test_helper'

class D3ChartDataControllerTest < ActionController::TestCase
  setup do
    @d3_chart_datum = d3_chart_data(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:d3_chart_data)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create d3_chart_datum" do
    assert_difference('D3ChartDatum.count') do
      post :create, d3_chart_datum: {  }
    end

    assert_redirected_to d3_chart_datum_path(assigns(:d3_chart_datum))
  end

  test "should show d3_chart_datum" do
    get :show, id: @d3_chart_datum
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @d3_chart_datum
    assert_response :success
  end

  test "should update d3_chart_datum" do
    put :update, id: @d3_chart_datum, d3_chart_datum: {  }
    assert_redirected_to d3_chart_datum_path(assigns(:d3_chart_datum))
  end

  test "should destroy d3_chart_datum" do
    assert_difference('D3ChartDatum.count', -1) do
      delete :destroy, id: @d3_chart_datum
    end

    assert_redirected_to d3_chart_data_path
  end
end
