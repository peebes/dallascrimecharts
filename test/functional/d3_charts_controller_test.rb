require 'test_helper'

class D3ChartsControllerTest < ActionController::TestCase
  setup do
    @d3_chart = d3_charts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:d3_charts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create d3_chart" do
    assert_difference('D3Chart.count') do
      post :create, d3_chart: {  }
    end

    assert_redirected_to d3_chart_path(assigns(:d3_chart))
  end

  test "should show d3_chart" do
    get :show, id: @d3_chart
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @d3_chart
    assert_response :success
  end

  test "should update d3_chart" do
    put :update, id: @d3_chart, d3_chart: {  }
    assert_redirected_to d3_chart_path(assigns(:d3_chart))
  end

  test "should destroy d3_chart" do
    assert_difference('D3Chart.count', -1) do
      delete :destroy, id: @d3_chart
    end

    assert_redirected_to d3_charts_path
  end
end
